<?php
    /*
    Template Name: CalendarAdmin
    */

    use Immoval\ImmovaSantaTemplate\Tools;

    define('DONOTCACHEPAGE', true);
    define('DONOTCACHEDB', true);
    global $post;
    $message = $link = '';

    if (file_exists(CURRENT_TEMPLATE . '/php_xlsxwriter/xlsxwriter.class.php')) {
        require_once(CURRENT_TEMPLATE . '/php_xlsxwriter/xlsxwriter.class.php');
    }
    
    if (isset($_POST['calendar_avent']) && !empty($_POST['calendar_avent'])) {
        $filename = str_replace(' ', '_', htmlspecialchars($_POST['calendar_avent']));

        try {
            $the_query = new WP_Query( ["post_type"=>"calendar"] );
            $nb_player = 0;

            $row = [
                'names' => $post->Names,
                'published_at' => $post->published_at,
                'meta_key' => [],
                'meta_value' => [],
            ];

            //Set SpreadSheet Title
            $posts = [
                array('PostID', 'Names', 'Name','Lastname','email','played_at'),
            ];

            $postIndex = 1;
            $postID = 0;
            while ( $the_query->have_posts() ) : $the_query->the_post();
                $posts [] = array(
                    $post->ID,
                    $post->post_title,
                    get_field( 'name', $post->ID ),
                    get_field( 'forname', $post->ID ),
                    get_the_date('d/m/Y H:i', $post->ID),
                    get_field( 'email', $post->ID ),
                );
            endwhile;
            wp_reset_postdata();

            $writer = new XLSXWriter();
            $writer->writeSheet($posts);
            $upload_dir = wp_get_upload_dir();
            $upload_filename = '/' . $filename . '.xlsx';
            $file_url = $writer->writeToFile('app/uploads' . $upload_filename);
            $link = $upload_dir['basedir'] . $upload_filename;

            if (file_exists($link)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($link).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($link));
                readfile($link);
                //Delete temp file
                //TODO - to comment on Production
                unlink($link);
                exit;
            }
        } catch (Exception $exception) {
            var_dump("ERROR : " . $exception->getMessage());
        }

    } else {
        $message = 'This field is required';
    }
?>
<?php get_header(); ?>

<main class="app-body layout-contact" role="main">

    <div class="container mb50" data-controller="calendar">
        <div class="block block-heading is-revealed" data-component="heading">
            <div class="container">
                <div class="heading">
                    <h2 class="heading__title">
                        <strong>Jeu de Calendrier de l'Avent <?=date('Y')?></strong>
                        Télécharment du resultat
                    </h2>
                </div>
            </div>
        </div>

        <form class="form form-celendar_avent" role="form" method="post" enctype="multipart/form-data">

            <input type="hidden" name="ID" value="<?= get_the_ID() ?>">

            <div class="row">
                <div class="form-group">
                    <div class="calendar_admin_avent">
                        <label for="calendar_avent" class="input-label">
                            <?php _e("Nom du fichier", 'immoval') ?> *
                        </label>
                        <input type="text" id="calendar_avent" name="calendar_avent" class="input-field"
                            placeholder="<?php _e("Saisissez le nom du fichier", 'immoval') ?>"
                            value="<?= (isset($_POST['calendar_avent'])) ? $_POST['calendar_avent'] : '' ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-submit">
                    <button class="btn-default btn--submit" type="submit">
                        <?php _e("Soumettre", 'immoval') ?>
                    </button>
                </div>
            </div>

        </form>

    </div>

</main>

<?php get_footer(); ?>