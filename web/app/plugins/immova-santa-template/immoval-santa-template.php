<?php
/*
Plugin Name: Immoval Santa Template
Plugin URI: https://plugins.backup-immoval.com/export_santa
Description: Plugin able to export Santas' player result into xlsx file.
Version: 1.0
Author: Immoval(KALUMVUATI Duramane)
*/

//Only for wordpress
if (! defined('ABSPATH')) {
    exit('You dont have to be here');
}

if ( !defined( 'FS_METHOD' ) ):
    define( 'FS_METHOD', 'direct' );
endif;

// Define Variables and Constantes
define("IMMOVAL_SANTA_TEMPLATE", plugin_dir_path(__FILE__));
define("CURRENT_TEMPLATE", get_theme_file_path());
const PAGE_MODEL  = IMMOVAL_SANTA_TEMPLATE."template/calendar_admin_template.php";

register_activation_hook( __FILE__, 'immoval_santa_template_activated' );
register_deactivation_hook( __FILE__, 'immoval_santa_template_deactivated' );

//Where finds copy_dir function
require_once ABSPATH . 'wp-admin/includes/file.php';
//------------------
require_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php';
WP_Filesystem(); // Set content into wp_filesystem

function immoval_santa_template_activated() {
    global $wp_filesystem;
    $theme_path = get_theme_file_path();
    $source = IMMOVAL_SANTA_TEMPLATE."vendor/mk-j";
    $destination = CURRENT_TEMPLATE .'/';
    if ( ! $wp_filesystem || ! is_object( $wp_filesystem ) ) {
        throw new Exception("Error Processing Request", 1);
    }
    //------------------
    if (file_exists(PAGE_MODEL) && file_exists($theme_path) && file_exists($source)) {
        //Copy PAGE_MODEL file to theme directory
        copy(PAGE_MODEL,  "$theme_path/calendar_admin_template.php" );
    
        //copy template file to theme
        if (is_dir($source) && is_dir($destination) && function_exists('copy_dir')) {
            
            try {
                //copy a package importance for this plugin works
                copy_dir($source, $destination);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }
}

//TODO - to delete all files and dires that we added on Activate Plugin
function immoval_santa_template_deactivated() {
    $error = (object) 'error';
    var_dump($error);
    // die("Top");
}