<?php 

namespace Immoval\ImmovaSantaTemplate;

class Tools {

    public static function field_value( $name, $default = null ) {
        return (isset($_POST[$name])) ? $_POST[$name] : $default;
    }
}