<?php

use Immoval\Plugins\Model\Export;
/*
Plugin Name: Immoval Santa Export
Plugin URI: https://plugins.backup-immoval.com/export_santa
Description: Plugin able to export Santas' player result into xlsx file.
Version: 1.0
Author: Immoval(KALUMVUATI Duramane)
*/

//Only for wordpress
if (! defined('ABSPATH')) {
    exit('You dont have to be here');
}

define("IMMOVAL_SANTA_PUGIN", plugin_dir_path(__FILE__));

require_once IMMOVAL_SANTA_PUGIN. 'vendor/autoload.php';

$export = new Export(__FILE__);