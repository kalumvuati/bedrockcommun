<?php 


namespace Immoval\Plugins\Controller;

use Immoval\Plugins\Model\Export;

class SantaExportAdminController {

    const REDIRECT_TO_LIST = 0;
    const REDIRECT_TO_EDIT = 1;

    public function __construct() {
        $this->init_hooks();
    }
        
    public function init_hooks() {
        add_action('admin_menu', [$this, 'immoval_santa_export_admin_menu']);
        add_action('admin_init', [$this, 'immoval_santa_export_admin_init']);
    }
    
    /**
     * this method will allow Admin to configurate Santa Export Page
     */
    public function immoval_santa_export_admin_menu() {
        // TODO - to remove Post word
        add_options_page(
            'Santa Post',
            'Santa Export',
            'manage_options',
            'santa_export_post',
            [$this, 'config_page']
        );
    }

    /**
     * Page Rended on Click Santa Export Option inside of wordpress Back-Office
     */
    public function config_page() {
        Export::render('admin/config_page', []);
    }
    
    public function immoval_santa_export_admin_init() {
       register_setting('general', 'santa_post_general');
       add_settings_section('santa_export_post_main', null, null, 'santa_export_post');
       add_settings_field('redirect_to', 'Rediriger vers', [$this, 'retirect_to_render'], 'santa_export_post', 'santa_export_post_main');
    }

    public function retirect_to_render() {
        $general_options = get_option('santa_post_general');
        $selectedValue = $general_options['redirect_to'];
        ?>
<select name="santa_post_general[redirect_to]">
    <option value="<?= self::REDIRECT_TO_EDIT ?>" <?= selected('redirect_to_edit', $selectedValue)?>>Edit</option>
    <option value="<?= self::REDIRECT_TO_LIST ?>" <?= selected('redirect_to_list', $selectedValue)?>>List</option>
</select>
<?php
    }

}