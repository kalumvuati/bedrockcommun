<?php

namespace Immoval\Plugins\Model;

use Immoval\Plugins\Controller\SantaExportAdminController;

class Export {

    const PLUGIN_START = 'immoval_santa_export';
    const POST_TYPE = 'santa_export';

    public function __construct(string $file) {
        add_action('init', [$this, 'immoval_santa_export_init']);
        if (is_admin()) {
            //Create Admin Page for manage Santa Export
            $santaExportAdmin = new SantaExportAdminController();
        }
    }
        
    /**
     * This method will be called on plugin activation
     */
    public function immoval_santa_export_init() {
        $labels = array(
            // Le nom au pluriel
            'name'                => _x( 'Santas Exports', 'Post Type General Name'),
            // Le nom au singulier
            'singular_name'       => _x( 'Santas Exports', 'Post Type Singular Name'),
            // Le libellé affiché dans le menu
            'menu_name'           => __( 'Santas Exports'),
            // Les différents libellés de l'administration
            'all_items'           => __( 'Toutes les Santa Export'),
            'view_item'           => __( 'Voir les Santa Export'),
            'add_new_item'        => __( 'Ajouter une nouvelle santa export'),
            'add_new'             => __( 'Ajouter'),
            'edit_item'           => __( 'Editer la Santa export'),
            'update_item'         => __( 'Modifier les Santas export'),
            'search_items'        => __( 'Rechercher une Santa export'),
            'not_found'           => __( 'Non trouvée'),
            'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
        );

        $args = array(
            'label'               => __( 'Santa Export'),
            'description'         => __( 'Tous sur Santas Export'),
            'labels'              => $labels,
            // 'supports'            => array( 'title', 'author', 'thumbnail', 'custom-fields' ),
            'show_in_rest' => true,
            'hierarchical'        => false,
            'public'              => true,
            'has_archive'         => true,
            'menu_position'         => 23,
            'menu_icon'             => 'dashicons-external',
            'rewrite'			  => array( 'slug' => 'santa_export'),
            'capabilities' => array(
                'create_posts' => false, 
              )
        );

        //Create a new Post type
        register_post_type(self::POST_TYPE, $args);
    }

    public static function render(string $filename, array $args = []) {
        
        extract($args);
        $file = IMMOVAL_SANTA_PUGIN."view/$filename.php";
        ob_start();
        require_once($file);
        echo ob_get_clean();
    }
}
