<div class="wrap">
    <h1 class="wp-heading-inline">Santa Export configuration</h1>
    <form action="options.php" method="post">
        <?= settings_fields('general') ?>
        <?= do_settings_sections('santa_export_post_main') ?>
        <?= submit_button() ?>
    </form>
</div>